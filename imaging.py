from datetime import datetime, timedelta, date
from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.pyson import Eval, If
from trytond.pool import Pool
from trytond.report import Report
from trytond.wizard import Wizard, StateTransition
from trytond.i18n import gettext
from trytond.exceptions import UserError
from trytond.transaction import Transaction


sequences = ['imaging_request_sequence', 'imaging_sequence']


class ResultTemplate(ModelSQL, ModelView):
    'Imaging Result Template'
    __name__ = 'health.imaging.result_template'
    name = fields.Char('Name', required=True)
    description = fields.Text('Description', required=True)
    conclusion = fields.Text('Conclusion')
    type_template = fields.Many2One('health.imaging.type_result_template', 'Type')



class TypeResultTemplate(ModelSQL, ModelView):
    'Imaging Test Type'
    __name__ = 'health.imaging.type_result_template'
    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)


class TestType(ModelSQL, ModelView):
    'Imaging Test Type'
    __name__ = 'health.imaging.test.type'
    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)


class Test(ModelSQL, ModelView):
    'Imaging Test'
    __name__ = 'health.imaging.test'
    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active', select=True)
    product = fields.Many2One('product.product', 'Product', required=True)
    test_type = fields.Many2One('health.imaging.test.type', 'Type')

    @staticmethod
    def default_active():
        return True


class ImagingTestRequest(Workflow, ModelSQL, ModelView):
    'Imaging Test Request'
    __name__ = 'health.imaging.request'
    STATES_REQUEST = {'readonly': Eval('state') != 'draft'}
    STATES_ANALYSIS = {'readonly': Eval('state') != 'in_analysis'}
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    patient = fields.Many2One('health.patient', 'Patient', required=True,
        states=STATES_REQUEST)
    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': Eval('state') != 'draft'}, depends=['state'])
    request_date = fields.DateTime('Request Date',
        states={'readonly': True})
    analysis_date = fields.DateTime('Analysis Date', states={'readonly': True})
    test = fields.Many2One('health.imaging.test', 'Test',
        required=True, states=STATES_REQUEST)
    physician = fields.Many2One('health.professional', 'Physician',
        required=False, help='Doctor who request service.',
        states={'readonly': True})
    radiologist = fields.Many2One('health.professional', 'Radiologist',
        states=STATES_ANALYSIS)
    images = fields.One2Many('ir.attachment', 'resource', 'Images',
        states=STATES_ANALYSIS)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('requested', 'Requested'),
        ('in_analysis', 'In Analysis'),
        ('cancelled', 'Cancelled'),
        ('done', 'Done'),
        ], 'State', readonly=True)
    urgent = fields.Boolean('Urgent', states={'readonly': True})
    findings = fields.Text('Findings', states=STATES_ANALYSIS,
        depends=['results_template'])
    conclusion = fields.Text('Conclusion', states=STATES_ANALYSIS,
        depends=['results_template'])
    comment = fields.Text('Comment', states=STATES_ANALYSIS)
    results_template = fields.Many2One('health.imaging.result_template',
        'Results Template', domain=[If (Eval('type_template'),
        ('type_template', '=', Eval('type_template')), ())], states=STATES_ANALYSIS)
    type_template = fields.Many2One('health.imaging.type_result_template', 'Type', states=STATES_ANALYSIS)
    admission_reason = fields.Many2One('health.pathology',
        'Reason for Admission', help="Reason for Admission", select=True,
        states=STATES_REQUEST)
    presuntive_diagnosis = fields.Many2One('health.pathology',
        'Presuntive Diagnosis', states={
            'readonly': Eval('state') != 'draft',
        }
    )

    @classmethod
    def __setup__(cls):
        super(ImagingTestRequest, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('number_uniq', Unique(t, t.number),
             'The test number code must be unique')
        ]
        cls._order.insert(0, ('request_date', 'DESC'))
        cls._transitions |= set((
            ('draft', 'requested'),
            ('requested', 'in_analysis'),
            ('in_analysis', 'done'),
            ('cancelled', 'draft'),
            ('draft', 'cancelled'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'cancelled',
            },
            'request': {
                'invisible': Eval('state') != 'draft',
            },
            'in_analysis': {
                'invisible': Eval('state') != 'requested',
            },
            'cancel': {
                'invisible': ~Eval('state').in_(['draft', 'requested']),
            },
            'done': {
                'invisible': Eval('state') != 'in_analysis',
            },
        })
        cls._order.insert(0, ('request_date', 'DESC'))
        cls._order.insert(1, ('number', 'DESC'))

    @classmethod
    @ModelView.button
    @Workflow.transition('requested')
    def request(cls, records):
        pass

    @classmethod
    @Workflow.transition('done')
    def done(cls, records):
        pass

    @classmethod
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @Workflow.transition('in_analysis')
    def in_analysis(cls, records):
        cls.write(records, {'analysis_date': datetime.now()})

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_request_date():
        return datetime.now()

    @staticmethod
    def default_physician():
        HealthProf = Pool().get('health.professional')
        try:
            return HealthProf.get_health_professional()
        except:
            pass

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        config = pool.get('health.configuration').get_config()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('number'):
                values['number'] = config.imaging_request_sequence.get()
            values['state'] = 'requested'
        return super(ImagingTestRequest, cls).create(vlist)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [
            bool_op,
            ('patient',) + tuple(clause[1:]),
            ('number',) + tuple(clause[1:]),
        ]

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.service', 'health.patient.evaluation']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @fields.depends('results_template')
    def on_change_results_template(self, name=None):
        if self.results_template:
            self.findings = self.results_template.description
            self.conclusion = self.results_template.conclusion

    def edit_diagnosis(self):
        # Set state back to in analysis.
        self.state = 'in_analysis'
        self.save()


class EditDiagnosis(Wizard):
    "Edit Diagnosis"
    __name__ = 'health.imaging.edit_diagnosis'
    start_state = 'edit_diagnosis'
    edit_diagnosis = StateTransition()

    @classmethod
    def __setup__(cls):
        super(EditDiagnosis, cls).__setup__()

    def transition_edit_diagnosis(self):
        Imaging = Pool().get('health.imaging.request')
        ids = Transaction().context['active_ids']

        for record in Imaging.browse(ids):
            if record.state!='done':
                raise UserError(gettext(
                    'health_imaging.msg_imaging_error_patient_not_done_diagnosis'
                ))
            if record.state=='done':
                record.edit_diagnosis()
        return 'end'

class ImagingDiagnosisReport(Report):
    'Imaging Diagnosis Report'
    __name__ = 'health_imaging.imaging_diagnosis_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ImagingDiagnosisReport, cls).get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        time_now = datetime.now() - timedelta(hours=5)
        report_context['hour'] = time_now
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context
