from datetime import datetime

from trytond.pool import Pool, PoolMeta
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateTransition, StateView


class ImagingRequestForm(ModelView):
    'Imaging Request Form'
    __name__ = 'health_imaging.service_imaging_request.form'
    patient = fields.Many2One('health.patient', 'Patient', required=True)
    tests = fields.Many2Many('health.imaging.test', None, None, 'Imaging Test',
        required=True)


class ImagingRequest(Wizard):
    'Imaging Test Request'
    __name__ = 'health_imaging.service_imaging_request'
    start = StateView('health_imaging.service_imaging_request.form',
        'health_imaging.service_imaging_request_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        ImagingRequest = pool.get('health.imaging.request')
        Service = pool.get('health.service')
        ServiceLine = pool.get('health.service.line')
        now = datetime.now()
        products = []
        imagings = []
        desc = 'Imag. Test'

        plan = None
        insurances = self.start.patient.party.insurance
        if insurances:
            plan = insurances[0].plan

        for test in self.start.tests:
            unit_price = ServiceLine.compute_price(test.product, plan)
            request, = ImagingRequest.create([{
                'patient': self.start.patient.id,
                'request_date': now,
                'test': test.id,
            }])

            desc += ' | ' + request.number
            imagings.append(request)
            products.append({
                'product': test.product.id,
                'unit_price': unit_price,
                'qty': 1,
                'to_invoice': True,
                'state': 'draft',
                'desc': test.rec_name,
                'origin': str(request),
            })
        to_create = {
            'patient': self.start.patient.id,
            'invoice_to': self.start.patient.party.id,
            'state': 'draft',
            'desc': desc,
            'lines': [('create', products)],
        }
        if plan:
            to_create['insurance_plan'] = plan.id
        service, = Service.create([to_create])
        ImagingRequest.write(imagings, {'origin': str(service)})
        return 'end'


class HealthServiceLine(metaclass=PoolMeta):
    __name__ = 'health.service.line'

    @classmethod
    def _get_origin(cls):
        return super(HealthServiceLine, cls)._get_origin() + [
            'health.imaging.request'
        ]
