from datetime import datetime

from trytond.pool import Pool, PoolMeta
from trytond.model import ModelView, fields
from trytond.pyson import Eval


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'health.patient.evaluation'
    STATES = {'readonly': Eval('state') == 'signed'}
    imaging_requests = fields.One2Many('health.imaging.request', 'origin',
        'Images Request', states=STATES,
            domain=[
                ('patient', '=', Eval('patient')),
            ], context={'patient': Eval('patient', -1)}
        )
