# -*- coding: utf-8 -*-
from datetime import datetime

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateAction, StateTransition, StateView, \
    Button
from trytond.transaction import Transaction
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool
from trytond.i18n import gettext
from trytond.exceptions import UserError


class WizardGenerateResult(Wizard):
    'Generate Results'
    __name__ = 'wizard.generate.result'
    start_state = 'open_'
    open_ = StateAction('health_imaging.act_imaging_test_result_view')

    def do_open_(self, action):
        pool = Pool()
        Request = pool.get('health.imaging.test.request')
        Result = pool.get('health.imaging.test.result')

        request_data = []
        requests = Request.browse(Transaction().context.get('active_ids'))
        for request in requests:
            request_data.append({
                'patient': request.patient.id,
                'date': datetime.now(),
                'request_date': request.date,
                'requested_test': request.requested_test,
                'request': request.id,
                'doctor': request.doctor})
        results = Result.create(request_data)

        action['pyson_domain'] = PYSONEncoder().encode(
            [('id', 'in', [r.id for r in results])])

        Request.requested(requests)
        Request.done(requests)
        return action, {}


class RequestImagingTest(ModelView):
    'Request - Test'
    __name__ = 'health.request-imaging-test'
    _table = 'health_request_imaging_test'

    request = fields.Many2One('health.patient.imaging.test.request.start',
        'Request', required=True)
    test = fields.Many2One('health.imaging.test', 'Test', required=True)


class RequestPatientImagingTestStart(ModelView):
    'Request Patient Imaging Test Start'
    __name__ = 'health.patient.imaging.test.request.start'

    date = fields.DateTime('Date')
    patient = fields.Many2One('health.patient', 'Patient', required=True)
    doctor = fields.Many2One('health.professional', 'Doctor',
        required=True, help="Doctor who Request the lab tests.")
    tests = fields.Many2Many('health.request-imaging-test', 'request',
        'test', 'Tests', required=True)
    urgent = fields.Boolean('Urgent')

    @staticmethod
    def default_date():
        return datetime.now()

    @staticmethod
    def default_patient():
        if Transaction().context.get('active_model') == 'health.patient':
            return Transaction().context.get('active_id')

    @staticmethod
    def default_doctor():
        hp = Pool().get('health.professional').get_health_professional()
        if not hp:
            raise UserError(gettext('health.msg_health_professional_warning'))
        return hp


class RequestPatientImagingTest(Wizard):
    'Request Patient Imaging Test'
    __name__ = 'health.patient.imaging.test.request'

    start = StateView('health.patient.imaging.test.request.start',
        'health_imaging.patient_imaging_test_request_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Request', 'request', 'tryton-ok', default=True),
            ])
    request = StateTransition()

    def transition_request(self):
        ImagingTestRequest = Pool().get('health.imaging.test.request')
        config = Pool().get('health.configuration').get_config()
        number = config.imaging_request_sequence.get()
        imaging_tests = []
        for test in self.start.tests:
            imaging_test = {}
            imaging_test['number'] = number
            imaging_test['requested_test'] = test.id
            imaging_test['patient'] = self.start.patient.id
            if self.start.doctor:
                imaging_test['doctor'] = self.start.doctor.id
            imaging_test['date'] = self.start.date
            imaging_test['urgent'] = self.start.urgent
            imaging_tests.append(imaging_test)
        ImagingTestRequest.create(imaging_tests)
        return 'end'
